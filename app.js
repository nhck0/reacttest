// let ob = {
//   name: "This is name",
//   surname: "This is surname",
//   age: 5,
//   getFullName: function () {
//       return this.name + " " + this.surname;
//   }
//
// };
//
// ReactDOM.render(
//     <h1>
//         <p className={"red"}>Name: {ob.name}</p>
//         <p className={"blue"}>Surname: {ob.surname}</p>
//         <p>age: {ob.age}</p>
//         <p>FullName: {ob.getFullName()}</p>
//     </h1>,  // элемент, который мы хотим создать
//     document.getElementById("app")    // где мы этот элемент хотим создать
// );

// class SayHi extends React.Component{
//     render(){
//         return <div>
//             <p>name: {this.props.name}</p>
//             <p>age: {this.props.age}</p>
//         </div>
//     }
// }
// SayHi.defaultProps = {name: "qwe", age: 55};
//
// ReactDOM.render(
//     <SayHi age="34"/>,
//     document.getElementById("app")
// );

// const comment = {
//     date: new Date().toLocaleDateString(),
//     text: "Learn React!",
//     author: {
//         name: "Author name",
//         AvatarURL: 'http://placekitten.com/g/64/64',
//     }
// };
// class Avatar extends React.Component{
//     render(){
//         return(
//             <div className="Avatar">
//                 <img src={this.props.user.AvatarURL} alt={this.props.user.name} className="Avatar-pic" />
//             </div>
//         )
//     }
// }
//
// class UserInfo extends React.Component{
//     render(){
//         return(
//             <div className="UserName">
//                 {this.props.user.name}
//             </div>
//         )
//     }
// }
//
// class CommentText extends React.Component{
//     render(){
//         return(
//             <div className="CommentText">
//                 {this.props.commentText}
//             </div>
//         )
//     }
// }
//
// class CurrentDate extends React.Component{
//     render(){
//         return(
//             <div className="currentDate">
//                 {this.props.currentDate}
//             </div>
//         )
//     }
// }
//
// class Comment extends React.Component{
//     render(){
//         return(
//             <div className="comment">
//                 <Avatar user={this.props.author} />
//                 <UserInfo user={this.props.author} />
//                 <CommentText commentText={this.props.text} />
//                 <CurrentDate currentDate={this.props.date} />
//             </div>
//         )
//     }
// }
//
// ReactDOM.render(
//     <Comment author={comment.author} text={comment.text} date={comment.date} />,
//     document.getElementById('app')
// );\

const summerOffer = {
    aboutOffer: {
        offerPhoto: 'http://placekitten.com/g/64/64',
        offerTopic: "offer topic",
        offerDesc: "offer description"
    },
    offerValue: {
        value: "700$",
        buttonText: "Read more"
    }
};

class OfferPhoto extends React.Component{
    render(){
        return(
            <img src={this.props.photo.offerPhoto} alt={this.props.photo.offerTopic}/>
        )
    }
}

class Offer extends React.Component{
    render(){
        return(
            <ul className="offers">
                <li>
                    <OfferPhoto photo={this.props.offerPhoto}/>
                </li>
            </ul>
        )
    }
}


ReactDOM.render(
    <Offer offerPhoto={summerOffer.aboutOffer}/>,
    document.getElementById("app")
);